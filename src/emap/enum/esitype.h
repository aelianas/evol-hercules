// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_ENUM_ESITYPE
#define EVOL_MAP_ENUM_ESITYPE

enum esi_type
{
    SI_PHYSICAL_SHIELD = 966,
};

#endif  // EVOL_MAP_ENUM_ESITYPE
